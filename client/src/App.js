import React, {Component} from 'react'
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import { Button } from 'reactstrap';

import PowerList from "./PowerList"

class App extends Component{

  constructor(props){
    super(props)
    this.state = {
      data: []
    }
  }

  componentDidMount() {
    console.log("hello")
    fetch("/api/items")
      .then(result => result.json())
      .then(data => {
        this.setState({data: data})
      })
  }

  render() {
    return (
      <div>
        <PowerList data={this.state.data}/>
        <Button>Hello</Button>
      </div>
    );
  }
}

export default App;
