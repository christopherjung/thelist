import React, { Component }  from 'react';

import { ListGroup, ListGroupItem, Container, Row, Col } from 'reactstrap';


class PowerList extends Component{
  render() {
    let {data:list} = this.props
    return (
      <Container>
        <Row>
          <Col>
            Das will ich machen!
            <ListGroup>
              {list.map(item => <ListGroupItem key={item}>{item}</ListGroupItem>)}
            </ListGroup>
          </Col>
          <Col>
            Das will ich futtern!
            <ListGroup>
              {list.map(item => <ListGroupItem key={item}>{item}</ListGroupItem>)}
            </ListGroup>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default PowerList
